<div class="row"> 
    <div class="col-xs-12 col-md-offset-4 col-md-4"> <!-- Bootstrappia, xs tarkottaa että kaikilla näytöillä -->
        <h3>Kirjaudu</h3>
        <?php
        print validation_errors();
        ?>
        <form role="form" method="post" action="<?php print site_url(). 'kayttaja/kirjaudu'?>">
            <div class="form-group">
                <label for="tunnus">Käyttäjätunnus:</label>
                <input type="text" name="tunnus" class="form-control">
            </div>
            <div class="form-group">
                <label for="salasana">Salasana:</label>
                <input type="password" name="salasana" class="form-control">
            </div>
            <button class="btn btn-primary">Kirjaudu</button>
            <a class="btn btn-default" href="<?php print site_url() . 'kayttaja/rekisteroityminen'?>">
                Rekisteröidy
            </a>
        </form>
    </div>
</div>