<form action="<?php print site_url() . '/muistio/tallenna'; ?>" method="post">
    <div class="form-group">
        <label>Aika</label>
        <input name="paivays" class="form-control" type="datetime" value="<?php print ($paivays); ?>">
    </div>
    <div class="form-group">
        <label>Teksti</label>
        <textarea name="teksti" class="form-control"></textarea>
    </div>
    <div>
        <button>Tallenna</button>
    </div>
</form>
<div>
    <?php
    foreach ($muistiot as $muistio) {
        print "<hr>";
        print "<p>$muistio->tallennettu</p>";
        print "<p>$muistio->teksti</p>";
    }
    ?>
</div>