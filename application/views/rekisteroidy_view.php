<div class="row"> 
    <div class="col-xs-12 col-md-offset-4 col-md-4"> <!-- Bootstrappia, xs tarkottaa että kaikilla näytöillä -->
        <h3>Rekisteröidy</h3>
        <?php
        print validation_errors();
        ?>
        <form role="form" method="post" action="<?php print site_url(). '/kayttaja/rekisteroidy'?>">
            <div class="form-group">
                <label for="tunnus">Käyttäjätunnus:</label>
                <input type="email" name="tunnus" class="form-control">
            </div>
            <div class="form-group">
                <label for="salasana">Salasana:</label>
                <input type="password" name="salasana" class="form-control">
            </div>
            <div class="form-group">
                <label for="salasana2">Salasana uudestaan:</label>
                <input type="password" name="salasana2" class="form-control">
            </div>
            <button class="btn btn-primary">Rekisteröidy</button>
            <a class="btn btn-default" href="<?php print site_url() . '/kayttaja/index'?>">
                Kirjaudu
            </a>
        </form>
    </div>
</div>