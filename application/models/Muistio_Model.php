<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muistio_Model extends CI_Model {
        public function __construct() {
                parent::__construct();
        }
    
        public function hae_kaikki($asiakas_id) {
            //select * from muistio where asiakas_id = $asiakas_id
                $this->db->where('asiakas_id',$asiakas_id);
                $query = $this->db->get('muistio');
                return $query->result();
        }
        
        public function hae($id) {
                $this->db->where('id',$id);
                $query = $this->db->get('muistio');
                return $query->row();
        }
        
        public function lisaa($data) {
                $this->db->insert('muistio',$data);
                return $this->db->insert_id();
        }
        
        public function muokkaa($data) {
                $this->db->where('id',$data['id']);
                $this->db->update('muistio',$data);
        }
        
        public function poista($id) {
                //DELETE FROM asiakas WHERE id = $id
                $this->db->where('id',$id);
                $this->db->delete('muistio');
        }
        
}