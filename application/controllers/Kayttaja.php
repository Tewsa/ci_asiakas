<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kayttaja extends CI_Controller {
    public function __construct() {  /* aina konstruktori */
        parent::__construct();
        $this->load->model('kayttaja_model');
        $this->load->library('encrypt'); /* lataa enkryptauskirjaston */
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['main_content'] = 'kirjaudu_view'; /* tämä hakee sivupohjan */
        $this->load->view('template',$data);
    }
    
    public function kirjaudu() {
        
        /* validointi tähän */
        $this->form_validation->set_rules('tunnus','tunnus','required|valid_email'); /* kayttäjätunnus pitää antaa, oltava sähköpostiosoite */
        $this->form_validation->set_rules('salasana','salasana','required|min_length[8]'); /* salasana pitää antaa, minimipituus 8 */
        $this->form_validation->set_rules('tunnus','salasana','callback_tarkasta_kayttaja');
        
        if($this->form_validation->run()===TRUE) {
            redirect('asiakas/index');
        }
        else {
            $data['main_content'] = 'kirjaudu_view';
            $this->load->view('template',$data);
        }
    }
    
    public function kirjaudu_ulos() {
        $this->session->unset_userdata('kayttaja'); /* koko istuntoa ei tuhota, istuntomuuttuja */
        $this->session->sess_destroy(); /* tuhoaa ehkä koko istunnon */
        
        redirect('kayttaja/index','refresh');
    }

    public function tarkasta_kayttaja() {
        $tunnus = $this->input->post('tunnus');
        $salasana = $this->input->post('salasana');
        
        $kayttaja = $this->kayttaja_model->tarkasta_kayttaja($tunnus,$salasana);
        
        if ($kayttaja) {
            $this->session->set_userdata('kayttaja',$kayttaja);
            return TRUE;
        }
        else {
            $this->form_validation->set_message('tarkasta_kayttaja','Käyttäjätunnus tai salasana virheellinen!');
            return FALSE;
        }
    }
    
    public function rekisteroityminen()
    {
        $data['main_content'] = 'rekisteroidy_view'; /* tämä hakee sivupohjan */
        $this->load->view('template',$data);
    }

    public function rekisteroidy() {
        $data = array(
            'email' => $this->input->post('tunnus'), /*  eka viittaa tietokannan tietoon, toka htmlssä mainittuun */
            'salasana' => $this->encrypt->encode($this->input->post('salasana')) /* enkryptaa */
        );

        /* validointi */
        $this->form_validation->set_rules('tunnus','tunnus','required|valid_email'); /* kayttäjätunnus pitää antaa, oltava sähköpostiosoite */
        $this->form_validation->set_rules('salasana','salasana','required|min_length[8]'); /* salasana pitää antaa, minimipituus 8 */
        $this->form_validation->set_rules('salasana2','salasana uudestaan','matches[salasana]');

        if($this->form_validation->run()===TRUE) {
            $this->kayttaja_model->lisaa($data);
            $this->index();
        }

        else {
            $data['main_content'] = 'rekisteroidy_view';
            $this->load->view('template',$data);
        }
    }
}